package com.code_house;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeHouseApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodeHouseApplication.class, args);
    }

}
