package com.code_house.auto_code;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.ArrayList;

/**
 * @authoor Bilysen
 * @date 2021/5/9 - 10:49
 **/
public class AutoCode {
    public static void main(String[] args) {
        // 1.代码生成器对象
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");   //D:\Bilysen MyBatis-Plus\mybatis_plus_autocode得到项目地址
        gc.setOutputDir(projectPath + "/src/main/java");   //生成文件的输出路径D:\Bilysen MyBatis-Plus\mybatis_plus_autocode\src\main\java
        gc.setAuthor("Bilysen");
        gc.setOpen(false);      //生成代码后是否打开文件夹
        gc.setFileOverride(false);  //是否覆盖
        gc.setServiceName("%sService");  // 设置Service接口生成名称,这样生成接口前面就不会有 I
        gc.setIdType(IdType.AUTO);     //主键id自增
        gc.setDateType(DateType.ONLY_DATE);  //时间类型对应策略
        gc.setSwagger2(true);  //开启swagger
        mpg.setGlobalConfig(gc);

        //设置数据源
        DataSourceConfig dc = new DataSourceConfig();
        dc.setDriverName("com.mysql.cj.jdbc.Driver");
        dc.setUrl("jdbc:mysql://localhost:3306/mybatis_plus?serverTimezone=CTT&useSSL=false&useUnicode=true&characterEncoding=utf-8");
        dc.setUsername("root");
        dc.setPassword("Bilysen0112");
        dc.setDbType(DbType.MYSQL);
        mpg.setDataSource(dc);

        //包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("com");
        pc.setController("controller");
        pc.setService("service");
        pc.setMapper("mapper");
        pc.setEntity("pojo");
        pc.setModuleName("code_house");
        mpg.setPackageInfo(pc);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setInclude("user");  //设置要映射的表
        strategy.setNaming(NamingStrategy.underline_to_camel);    //表映射到实体的命名 下划线转驼峰命名
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);   //字段映射到实体的命名
        strategy.setEntityLombokModel(true);    //自动lombok
        strategy.setLogicDeleteFieldName("deleted");    //逻辑删除

        //自动填充配置
        TableFill createDate = new TableFill("create_date", FieldFill.INSERT);
        TableFill modifyDate = new TableFill("modify_date", FieldFill.INSERT_UPDATE);
        ArrayList<TableFill> tableFills = new ArrayList<>();
        tableFills.add(createDate);
        tableFills.add(modifyDate);
        strategy.setTableFillList(tableFills);

        //乐观锁
        strategy.setVersionFieldName("version");

        //生成 @RestController 控制器
        strategy.setRestControllerStyle(true);
        mpg.setStrategy(strategy);

        //执行代码生成器
        mpg.execute();
    }
}
