package com.code_house.mapper;

import com.code_house.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Bilysen
 * @since 2021-05-09
 */
public interface UserMapper extends BaseMapper<User> {

}
