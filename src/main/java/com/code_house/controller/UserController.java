package com.code_house.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Bilysen
 * @since 2021-05-09
 */
@RestController
@RequestMapping("/code_house/user")
public class UserController {
    @GetMapping("/hello")
    public String Hello(){
        return "hello";
    }
}

