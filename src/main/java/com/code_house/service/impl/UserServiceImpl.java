package com.code_house.service.impl;

import com.code_house.pojo.User;
import com.code_house.mapper.UserMapper;
import com.code_house.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Bilysen
 * @since 2021-05-09
 */
@Service
public class UserServiceImpl implements UserService {

}
